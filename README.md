Mediator, a javascript plugin for Vanilla
-----------------------------------------

Mediator is a [plugin](http://vanillaforums.org/addon/browse/plugins) written in Javascript for [Vanilla](http://vanillaforums.org/), an open-source forum software written in PHP.

Mediator converts embeddable resource urls to embedded resources. The current media supported are:

- ![](https://plus.google.com/_/favicon?domain=youtube.com) Youtube
- ![](https://plus.google.com/_/favicon?domain=vimeo.com) Vimeo
- ![](https://plus.google.com/_/favicon?domain=imgur.com) Imgur
- ![](https://plus.google.com/_/favicon?domain=gyazo.com) Gyazo
- ![](https://plus.google.com/_/favicon?domain=pastebin.com) Pastebin
- ![](https://plus.google.com/_/favicon?domain=soundcloud.com) Soundcloud
- ![](https://plus.google.com/_/favicon?domain=coub.com) Coub
